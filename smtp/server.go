package smtp

import (
	"bufio"
	"crypto/tls"
	"io"
	"net"
	"time"
)

type Option struct {
	Hostname    string
	UseTls      bool
	KeyFile     string
	CertFile    string
	MaxSize     uint
	ReadTimeout time.Duration
	Message     map[string]*Message
}

const (
	BUFSIZE = 262144
)

type conn struct {
	rw     net.Conn
	brw    *bufio.ReadWriter
	server *Server
}

func NewConn(rw net.Conn, server *Server) *conn {
	return &conn{
		rw:     rw,
		brw:    bufio.NewReadWriter(bufio.NewReader(rw), bufio.NewWriter(rw)),
		server: server,
	}
}

type Server struct {
	Addr    string
	Handler func(*Request) *Message
	Option  *Option
}

func NewServer() *Server {
	return &Server{}
}

func (srv *Server) ListenAndServe() error {
	addr := srv.Addr
	if addr == "" {
		addr = ":smtp"
	}
	l, e := net.Listen("tcp", addr)
	if e != nil {
		return e
	}
	return srv.Serve(l)
}

func (srv *Server) Serve(l net.Listener) error {
	defer l.Close()
	for {
		rw, e := l.Accept()
		if e != nil {
			return e
		}
		if srv.Option.ReadTimeout != 0 {
			rw.SetReadDeadline(time.Now().Add(srv.Option.ReadTimeout))
		}
		c := NewConn(rw, srv)
		go c.serve()
	}
	panic("not reached")
}

func (c *conn) close() {
}

func (c *conn) serve() {
	defer c.rw.Close()
	r := NewRequest(c.server.Option)
	c.write([]byte(r.connect().String()))

	for {
		input, _, e := c.brw.ReadLine()
		if e != nil {
			if e == io.EOF {
				return
			} else if neterr, ok := e.(net.Error); ok && neterr.Timeout() {
				goto ERR
			}
			return
		}
		if c.server.Option.ReadTimeout != 0 {
			c.rw.SetReadDeadline(time.Now().Add(c.server.Option.ReadTimeout))
		}

		m := r.handle(input)
		c.write([]byte(m.String()))

		if m.Code == 221 {
			return
		} else if m.Code == 220 { // STARTTLS
			cert, err := tls.LoadX509KeyPair(
				c.server.Option.CertFile,
				c.server.Option.KeyFile,
			)
			if err != nil {
				panic(err)
			}
			config := &tls.Config{
				NextProtos:   []string{"smtp"},
				Certificates: []tls.Certificate{cert},
			}
			tlsconn := tls.Server(c.rw, config)
			tlsconn.Handshake()
			// redefine brw with tls connection
			c.brw = bufio.NewReadWriter(bufio.NewReader(tlsconn), bufio.NewWriter(tlsconn))
		}
		if m.Code != 354 {
			continue
		}

		for {
			data := make([]byte, BUFSIZE)
			br, e := c.brw.Read(data)
			if e != nil {
				if e == io.EOF { // hang up
					return
				} else if neterr, ok := e.(net.Error); ok && neterr.Timeout() {
					goto ERR
				}
				return
			}
			if c.server.Option.ReadTimeout != 0 {
				c.rw.SetReadDeadline(time.Now().Add(c.server.Option.ReadTimeout))
			}
			if !r.receiveData(data[0:br]) {
				continue
			}
			completeMessage := c.server.Handler(r)
			c.write([]byte(completeMessage.String()))
			break
		}
	}
ERR:
	c.write([]byte(c.server.Option.Message["TIMEOUT"].String()))
	return
}

func (c *conn) write(packet []byte) {
	c.brw.Write(packet)
	c.brw.Flush()
}

func ListenAndServe(addr string, handler func(*Request) *Message, option *Option) {
	server := &Server{
		Addr:    addr,
		Handler: handler,
		Option:  option,
	}
	server.ListenAndServe()
}
