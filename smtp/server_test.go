package smtp_test

import (
	"fmt"
	smtp "go-smtp-server/smtp"
	"os"
	"testing"
)

func handler(r *smtp.Request) *smtp.Message {
	fmt.Println(string(r.Mailfrom))
	for _, v := range r.Rcptto {
		fmt.Println(string(v))
	}
	fmt.Println(string(r.Content))
	// a queued message is customizable
	return smtp.NewMessage(250, "OK queued id <queue id>")
}

func TestServe(t *testing.T) {
	hostname, _ := os.Hostname()
	smtp.ListenAndServe("127.0.0.1:6000", handler, &smtp.Option{
		Hostname:    hostname,
		UseTls:      true,
		KeyFile:     "server.key",
		CertFile:    "server.crt",
		MaxSize:     41943040,
		ReadTimeout: 10e9,
		Message: map[string]*smtp.Message{
			"NORMALOK":      smtp.NewMessage(250, "OK"),
			"VERIFY":        smtp.NewMessage(252, "please send some mail"),
			"CLOSE":         smtp.NewMessage(221, "closing connection"),
			"INVALIDSYNTAX": smtp.NewMessage(501, "invalid syntax"),
			"MAILFIRST":     smtp.NewMessage(503, "MAIL first"),
			"RCPTFIRST":     smtp.NewMessage(503, "RCPT first"),
			"GOAHEAD":       smtp.NewMessage(354, "go ahead"),
			"TIMEOUT":       smtp.NewMessage(554, "timeout closed"),
			"STARTTLS":      smtp.NewMessage(220, "Ready to start TLS"),
		},
	})
}
