package smtp

import (
	"fmt"
)

type Message struct {
	Code         uint
	Text         string
	ExtraMessage string
}

func NewMessage(code uint, text string) *Message {
	return &Message{
		Code:         code,
		Text:         text,
		ExtraMessage: "",
	}
}

func (m *Message) String() string {
	return m.ExtraMessage + fmt.Sprintf("%d %s\r\n", m.Code, m.Text)
}

func (m *Message) addMessage(code uint, text string) {
	m.ExtraMessage += fmt.Sprintf("%d-%s\r\n", code, text)
}
