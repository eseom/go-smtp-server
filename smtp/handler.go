package smtp

import (
	"bytes"
	"strconv"
	"strings"
)

// Request structure
type Request struct {
	transactionStarted bool
	Mailfrom           []byte
	Rcptto             [][]byte
	Content            []byte
	Option             *Option
}

func NewRequest(option *Option) *Request {
	return &Request{
		Option: option,
	}
}

func (r *Request) findElement(sep []byte, s [][]byte) bool {
	for _, v := range s {
		if string(v) == string(sep) {
			return true
		}
	}
	return false
}

func (r *Request) handle(line []byte) (m *Message) {
	var (
		command string
		param   []byte
	)
	pos := bytes.Index(line, []byte(" "))
	if pos == -1 {
		command = strings.ToUpper(
			strings.Trim(string(line), " "))
	} else {
		command = strings.ToUpper(
			strings.Trim(string(line[0:pos]), " "))
		param = bytes.TrimSpace(bytes.ToLower(line[pos:]))
	}

	switch command {
	case "MAIL":
		m = r.mail(param)
	case "RCPT":
		m = r.rcpt(param)
	case "DATA":
		m = r.data()
	case "NOOP":
		m = r.Option.Message["NORMALOK"]
	case "VRFY":
		m = r.Option.Message["VERIFY"]
	case "RSET":
		m = r.rset()
	case "HELP":
		m = r.Option.Message["NORMALOK"]
	case "HELO":
		m = r.Option.Message["NORMALOK"]
	case "EHLO":
		m = NewMessage(250, "ENHANCEDSTATUSCODES")
		m.addMessage(250, r.Option.Hostname)
		m.addMessage(250, "SIZE "+strconv.Itoa(int(r.Option.MaxSize)))
		m.addMessage(250, "8BITMIME")
		m.addMessage(250, "PIPELINING")
		if r.Option.UseTls {
			m.addMessage(250, "STARTTLS")
		}
	case "QUIT":
		m = r.Option.Message["CLOSE"]
	case "STARTTLS":
		if r.Option.UseTls {
			m = r.Option.Message["STARTTLS"]
		} else {
			m = r.Option.Message["INVALIDSYNTAX"]
		}
	default:
		m = r.Option.Message["INVALIDSYNTAX"]
	}
	return m
}

func (r *Request) mail(arg []byte) *Message {
	addr := r.extractAddr(arg)
	if addr == nil {
		return r.Option.Message["INVALIDSYNTAX"]
	}
	r.transactionStarted = true
	r.Mailfrom = addr
	r.Rcptto = [][]byte{}
	return r.Option.Message["NORMALOK"]
}

func (r *Request) rcpt(arg []byte) *Message {
	if !r.transactionStarted {
		return r.Option.Message["NORMALOK"]
	}
	addr := r.extractAddr(arg)
	if addr == nil {
		return r.Option.Message["INVALIDSYNTAX"]
	}
	if !r.findElement(addr, r.Rcptto) {
		r.Rcptto = append(r.Rcptto, []byte(addr))
	}
	return r.Option.Message["NORMALOK"]
}

func (r *Request) data() *Message {
	if !r.transactionStarted {
		return r.Option.Message["MAILFIRST"]
	}
	if len(r.Rcptto) < 1 {
		return r.Option.Message["RCPTFIRST"]
	}
	return r.Option.Message["GOAHEAD"]
}

func (r *Request) rset() *Message {
	r.transactionStarted = false
	r.Rcptto = [][]byte{}
	return r.Option.Message["NORMALOK"]
}

func (r *Request) receiveData(arg []byte) (complete bool) {
	offset := len(arg)
	if offset == 3 && string(arg) == ".\r\n" {
		return true
	}
	if offset >= 5 &&
		string(arg[offset-5:offset]) == "\r\n.\r\n" {
		complete = true
		offset = offset - 3
	}
	r.Content = append(r.Content, arg[0:offset]...)
	return complete
}

func (r *Request) connect() *Message {
	return NewMessage(220, r.Option.Hostname+" ESMTP service ready")
}

func (r *Request) extractAddr(arg []byte) (addr []byte) {
	p := bytes.SplitN(arg, []byte(":"), 2)
	if len(p) < 2 {
		return nil
	}
	t1 := bytes.Trim(p[1], " ")
	if len(t1) < 0 {
		return nil
	}
	addr = bytes.Trim(bytes.Trim(bytes.Trim(t1, "<"), ">"), " ")
	return addr
}
